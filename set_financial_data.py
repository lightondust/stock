from data_operation import DataOperation
from crawling import Crawling

op = DataOperation()
codes = op.get_stock_list()
#codes=[3665]

crawling = Crawling(mode="debug")
for code in codes:
    data = crawling.crawling_financial_data(code)
    op.set_financial_data(data)
    
crawling.br.quit()
