from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import time
import re

class Crawling():
    def __init__(self, mode=""):
        self.dir_v = "data"

        if mode=="debug":
            self.br = webdriver.Chrome()
        else:
            options = Options()
            options.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
            options.add_argument('--headless')
            self.br = webdriver.Chrome(chrome_options=options)

    def crawling_price(self, code):
        url="https://minkabu.jp/stock/{}"
        self.br.get(url.format(code))
        time.sleep(1)
        price_locator = "//*[@class='stock_price']"
        per_locator = "//th[contains(text(),'PER')]/following-sibling::*[contains(text(),'倍')]"
        date_locator = "//h2[contains(@class,'stock_label')][contains(text(),'株価')]/parent::*"
        try:
            price = self.br.find_element_by_xpath(price_locator).text.replace(",","").replace(" ","").replace("円","")
        except:
            price = "null"
        try:
            per = self.br.find_element_by_xpath(per_locator).text[:-1]
        except:
            per = "null"
        try:
            date_pre = self.br.find_element_by_xpath(date_locator).text      
            date = date_pre[date_pre.find("(")+1:date_pre.find(")")]
        except:
            date="01/01"
        return {"code":code, "price":price, "per":per, "date":date}

    def crawling_stock_list(self):
        url = "http://kabu-data.info/all_code/all_code_code.htm"
        locator = "//tbody/tr/td[1]"

        self.br.get(url)
        time.sleep(5)
        els = self.br.find_elements_by_xpath(locator)

        file = open(os.path.join(self.dir_v,"code.txt"),"w")
        for el in els:
            print(el.text,file=file)
        file.close()

    def crawling_financial_data(self, code):
        url="http://ke.kabupro.jp/xbrl/{}.htm"
        br = self.br
        br.get(url.format(code))
        result = { "code":code }
        result["data"] = {}

        def get_years(text):
            search_re = re.search(r"\d{4}", text)
            return text[search_re.start():search_re.end()]
        
        xpath = "//*[contains(@class,'CellName')]//*[contains(text(),'通期')]"

        texts = list(map(lambda el: el.text, br.find_elements_by_xpath(xpath)))
        years = list(map(get_years, texts))
        result["years"] = years
        
        xpath_v = xpath + "[contains(text(),'{}')]" + "/parent::*/following-sibling::*[{}]"

        for year in years:
            sales = br.find_element_by_xpath(xpath_v.format(year, 1)).text.replace(",","")
            operating_profit = br.find_element_by_xpath(xpath_v.format(year, 2)).text.replace(",","")
            net_income = br.find_element_by_xpath(xpath_v.format(year, 3)).text.replace(",","")
            result["data"][year] = {"sales":sales, "operating_profit":operating_profit, "net_income" : net_income}
        
        return result        

