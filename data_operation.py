from database_sql import Database
import os
from crawling import Crawling
import datetime


class DataOperation():

    def __init__(self):
        self.db = Database()
        self.dir_v = "data"

    def get_stock_list(self, code_from=0, code_to=10000):
        """
        証券番号一覧取得、ソートで取得してリストで返す、code_fromとcode_to指定で取得範囲指定可能
        """
        self.db.sql = "select code from stock.stock_list " + \
            "where status='defaut' and code > {code_from} and code < {code_to} order by code asc;"
        self.db.sql = self.db.sql.format(code_from=code_from, code_to=code_to)
        result_list = self.db.execute()
        stock_list = []
        for item in result_list:
            stock_list.append(item["code"])
        return stock_list

    def get_per(self, date="", positive_only=True):
        if not date:
            today = datetime.date.today()
            date = "{}/{}/{}".format(today.year,today.month,today.day)
        if positive_only:
            self.db.sql = "select per from stock.price where date='{}' and per>0 order by per;".format(date)
        pers = self.db.execute()
        per_list = []
        for per in pers:
            per_list.append(per["per"])
        return per_list

    def set_price(self, codes=[], mode=""):
        """
        証券番号を渡して、株価とPERをセットする
        """
        if not codes:
            codes = self.get_stock_list()

        crawling = Crawling(mode=mode)

        for code in codes:
            """
            株価情報の取得
            """
            price = crawling.crawling_price(code)
            
            """
            データ整理：日付処理、当日と翌日の場合分け
            """
            today = datetime.date.today()
            if ":" in price["date"]:
                price["date"] = "{year}/{month}/{day}".format(year = today.year, 
                                                    month = today.month, 
                                                    day = today.day)
            else:
                price["date"] = str(today.year)+"/"+price["date"]

            """
            データ整理：id=code+date
            """
            price["id"] = str(price["code"])+price["date"]

            """
            データ挿入、insert失敗の場合、updateする
            """
            self.db.sql = "insert into stock.price (code, last, per, date, id) values " + \
                            "({code}, {last}, {per}, '{date}', '{id}')"
            self.db.sql = self.db.sql.format( code = price["code"],
                                            last = price["price"],
                                            per = price["per"],
                                            date = price["date"],
                                            id = price["id"])

            if self.db.execute():
                pass
            else:
                self.db.sql="update stock.price set " + \
                            "code={code}, last={last}, per={per}, date='{date}' where id= '{id}'"
                self.db.sql = self.db.sql.format(code = price["code"],
                                                last = price["price"],
                                                per = price["per"],
                                                date = price["date"],
                                                id = price["id"])
                print("update {}".format(price["id"]))
                if self.db.execute():
                    pass
                else:
                    print("{}: error occurs".format(price["id"]))
                    print(self.db.sql)

        crawling.br.quit()

    def set_stock_list(self):
        """
        証券番号一覧のセット
        """
        file = open(os.path.join(self.dir_v,"code.txt"), "r")
        lines = file.readlines()
        for line in lines:
            if line:
                self.db.sql = "insert into stock.stock_list (code) values({});".format(line.strip())
                self.db.execute()

    def set_financial_data(self, data):
        """
        財務データセット
        """

        sql = "insert into stock.financial_data (id, code, year, sales, operating_profit, net_income) " + \
                "values({id}, {code}, {year}, {sales}, {operating_profit}, {net_income});"        
        db = self.db

        code = data["code"]
        for year in data["years"]:
            id_v = "{}{}".format(code, year)
            data_year = data["data"][year]
            
            db.sql = sql.format(id = id_v, 
                                code = code, 
                                year = year, 
                                sales = data_year["sales"], 
                                operating_profit = data_year["operating_profit"], 
                                net_income = data_year["net_income"])
            db.execute()

            print(db.sql)



